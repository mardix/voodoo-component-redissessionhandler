<?php

/**
 * RedisSessionHandler
 * 
 * @name Redis
 * @author Mardix
 * @since   Aug 28, 2013
 * 
 * Lib to expose PHP internal session to save in Redis for distributed system
 */
namespace Voodoo\Component;

use Voodoo;
use SessionHandlerInterface;
use Voodoo\Component\Model\Redis;

class RedisSessionHandler implements SessionHandlerInterface
{
    public $ttl = 1800; // 30 minutes default
    protected $db;
    protected $prefix = "PHPSESSID:";
 
    public static function register($prefix = null, $ttl = null, $db = null)
    {
        $config = Voodoo\Core\Config::Component()->get("RedisSessionHandler");
        $hostname = Voodoo\Core\Env::getHostName();
        $ttl = $config["ttl"];
        $prefix = $config["sessionName"] . ":" . $hostname . ":";
        $dbAlias = $config["dbAlias"];
        $db = Redis::connect($dbAlias);
        $redisSession = new self($prefix, $ttl, $db);
        session_set_save_handler($redisSession);  
        session_start();
    }
    
/*******************************************************************************/
    /**
     * 
     * @param type $prefix
     * @param type $ttl
     * @param type $db
     */
    public function __construct($db, $prefix = null, $ttl = null) 
    {
        $this->db = $db;
        if ($prefix) {
            $this->prefix = $prefix;
        }
        if ($ttl) {
            $this->ttl = $ttl;
        }
    }
 
    
    public function open($savePath, $sessionName) 
    {
        // No action necessary because connection is injected
        // in constructor and arguments are not applicable.
    }
 
    public function close() 
    {
        $this->db = null;
        unset($this->db);
    }
 
    public function read($id) 
    {
        $id = $this->prefix . $id;
        $sessData = $this->db->get($id);
        $this->db->expire($id, $this->ttl);
        return $sessData;
    }
 
    public function write($id, $data) 
    {
        $id = $this->prefix . $id;
        $this->db->set($id, $data);
        $this->db->expire($id, $this->ttl);
    }
 
    public function destroy($id) 
    {
        $this->db->del($this->prefix . $id);
    }
 
    public function gc($maxLifetime) 
    {
        // no action necessary because using EXPIRE
    }

}